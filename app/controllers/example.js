const { Article } = require('../models');

exports.getAll = async (req, res, next) => {
    try {
        let data = await Article.find({
            language: req.__language
        });
        req.__id_resource = 'all';
        res.__response(req, res, next, 'REQUEST_SUCCESSFUL', data);
    } catch (error) {
        next(error);
    }
};

exports.getOne = async (req, res, next) => {
    try {
        let data = await Article
            .findOne({ uuid: req.params.uuid })
            .orFail(new Error('ENTITY_NOT_EXISTS'));
        req.__id_resource = req.params.uuid;
        res.__response(req, res, next, 'REQUEST_SUCCESSFUL', data);
    } catch (error) {
        next(error);
    }
};

exports.create = async (req, res, next) => {
    try {
        let data = await Article.create(req.body);
        req.__id_resource = data.uuid;
        res.__response(req, res, next, 'RESOURCE_CREATED_SUCCESSFUL', data);
    } catch (error) {
        next(error);
    }
};

exports.update = async (req, res, next) => {
    try {
        await Article
            .findOneAndUpdate({ uuid: req.params.uuid }, req.body, {
                runValidators: true,
                context: 'query',
                new: true
            }).orFail(new Error('ENTITY_NOT_EXISTS'));
        req.__id_resource = req.params.uuid;
        res.__response(req, res, next, 'RESOURCE_UPDATED_SUCCESSFUL');
    } catch (error) {
        next(error);
    }
};

exports.delete = async (req, res, next) => {
    try {
        await Article
            .findOneAndDelete({ uuid: req.params.uuid })
            .orFail(new Error('ENTITY_NOT_EXISTS'));
        req.__id_resource = req.params.uuid;
        res.__response(req, res, next, 'RESOURCE_DELETED_SUCCESSFUL');
    } catch (error) {
        next(error);
    }
};

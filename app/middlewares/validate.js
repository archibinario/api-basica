//const { validator } = require('../services/validators');

exports.validate = async (req, res, next) => {
    try {
        // call to validate payload ...   await validator(req);
        next();
    } catch (error) {
        next(error);
    }
};

const { nanoid } = require('nanoid/async');
const { language } = require('../services/language');
const { uriValidator } = require('../services/validators');


exports.responseMode = async (req, res, next) => {
    try {
        await language(req);
        //...
        next();
    } catch (error) {
        next(error);
    }
};

exports.registerRequest = async (req, res, next) => {
    req.__id_request = req.get('x-request-id') || await nanoid(50);
    req.__fingerprint = req.get('x-fp') || req.get('user-agent');
    // ...
    next();
};

exports.checkURI = async (req, res, next) => {
    try {
        await uriValidator(req.originalUrl);
        next();
    } catch (error) {
        next(error);
    }
};

exports.checkMaintenance = async (req, res, next) => {
    try {
        // ...
        next();
    } catch (error) {
        next(error);
    }
};

exports.checkCache = async (req, res, next) => {
    try {
        // ...
        next();
    } catch (error) {
        next(error);
    }
};


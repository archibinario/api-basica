//const jwt = require('../services/jwt');
const { validator } = require('../services/validators');

exports.auth = async (req, res, next) => {
    try {
        //let session = await jwt.validateJWT(req);
        await validator(req);
        req.__user = {
            //
        };
        // await checkPermissions(req);
        next();
    } catch (error) {
        next(error);
    }
};

function checkPermissions(req) {
    return new Promise( async (resolve, reject) => {
        try {
            for (const permission of req.__user.permissions) {
                if (permission.path === req.__clean_url.path && permission.methods.includes(req.__clean_url.method)) {
                    return resolve();
                }
            }
            throw new Error('FORBIDDEN');
        } catch (err) {
            reject(err);
        }
    });
}

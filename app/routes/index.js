const Router = require('express').Router();

const { validate } = require('../middlewares/validate');
//const { auth } = require('../middlewares/auth');
const {
    // CORE
    // ...
    // BUSINESS
    ___exampleController } = require('../controllers');


Router.get('/example', validate, ___exampleController.getAll);
Router.get('/example/:id', validate, ___exampleController.getOne);
Router.post('/example', validate, ___exampleController.create);
Router.put('/example/:id', validate, ___exampleController.update);
Router.delete('/example/:id', validate, ___exampleController.delete);


module.exports = Router;

const bcrypt = require('bcrypt');

const { customAlphabet } = require('nanoid/async');
const { TOKEN_LENGTH, SALT } = require('../config').SECURITY;


exports.generateToken = (dictionary = 'alfanumeric', length = TOKEN_LENGTH ) => {
    return new Promise( async (resolve, reject) => {
        try {
            let nanoid = customAlphabet(alphabet(dictionary), length);
            let token = await nanoid();
            resolve(token);
        } catch(error) {
            reject(error);
        }
    });
};

function alphabet(id) {
    const list = new Map([
        ['numeric', '1234567890'],
        ['alfanumeric', '1234567890abcdefghijklmnopqrstuvwzyzABCDEFGHIJKLMNOPQRSTUVWXZY'],
        ['alfa', 'abcdefghijklmnopqrstuvwzyzABCDEFGHIJKLMNOPQRSTUVWXZY'],
        ['secure', '1234567890abcdefghijklmnopqrstuvwzyzABCDEFGHIJKLMNOPQRSTUVWXZY_-?¿=()/&%$#@!+*[]çÇ{}.:,;<>~'],
        ['exclude', '23456789abcdefghijkmnopqrstuvwzyzABCDEFGHJKLMNPQRSTUVWXZY'],
    ]);
    if (list.has(id)) {
        return list.get(id);
    } else {
        return list.get('alfa');
    }
}

exports.hashPassword = (password) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result = bcrypt.hash(password, SALT);
            resolve(result);
        } catch (error) {
            reject(error);
        }
    });
};

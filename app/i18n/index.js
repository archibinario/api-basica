const es = require('./es.json');
const en = require('./en.json');
const jp = require('./jp.json');

exports.translations = {
    'es-ES': {...es},
    'en-US': {...en},
    'jp-JP': {...jp}
};

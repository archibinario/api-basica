const { storeInCache } = require('../cache');
const { translations } = require('../../i18n');
const { RESPONSE_MODE_AS_CONSTANT } = require('../../config').LANGUAGE;

function getResponse (message = 'REQUEST_SUCCESSFUL', __language, __response_mode) {
    let response = translations[__language]['SUCCESS'][message];
    return {
        code: response.code,
        message: response.message
    };
}

// TODO hacer esto mas adaptable a las rutas reales
function isCacheable(url) {
    // ...
    return result;
}

exports.successResponse = async (req, res, next, result = 'REQUEST_SUCCESSFUL', data = null, postprocess = null) => {
    try {
        let { code, message } = await getResponse(result, req.__language, req.__response_mode);
        let response = {
            error: false,
            code: code,
            url: req.originalUrl,
            method: req.method,
            message: message
        };
        if (postprocess) {
            response.postprocess = postprocess;
        }
        if (data) {
            response.data = data;
        }
        if (isCacheable(req.originalUrl) && req.method.toLowerCase() === 'get' && !req.__is_cached) {
            await storeInCache(data, req.originalUrl);
        }
        res.set('Language', req.__language);
        global.__logger.info({
            msg: result,
            id_request: req.__id_request,
            id_resource: req.__id_resource,
            id_user: req.__user ? req.__user.user_uuid : 'not_logged'
        });
        return res.status(response.code).json(response);
    } catch (error) {
        next(error);
    }
};

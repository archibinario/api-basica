exports.MESSAGES = {
    'is_required': {
        'any.required': 'REQUIRED',
        'string.empty': 'REQUIRED',
        'any.empty': 'REQUIRED'
    }
};

exports.USER_CONSTRAINT = {
    TITLE: {
        ENUM: [
            'Mr',
            'Mrs',
            'Ms',
            'Miss',
            'Dr',
            'Prof',
            'Rev',
            'Sir',
            'Dame',
            'Lady',
            'Lord'
        ],
    },
    FIRST_NAME: { LEN: 60 }
    // ...
};

const Joi = require('@hapi/joi');
const { MESSAGES } = require('../messages');

module.exports = {
    params: Joi.object({
        id: Joi.string().regex(/^[a-f\d]{24}$/i).messages(MESSAGES.id),
        token: Joi.string().trim().regex(/^[A-Za-z0-9]{72}$/).messages(MESSAGES.token_validate_account),
        uuid: Joi.string().trim().regex(/^[A-Za-z\d-_]{21}$/).messages(MESSAGES.uuid)
    })
};

const Joi = require('@hapi/joi');
const { MESSAGES } = require('../messages');

module.exports = {
    headers: Joi.object({
        api_key: Joi.string().regex(/^[A-Za-z\d-_]{64}$/i).messages(MESSAGES.format_not_valid),
        api_secret: Joi.string().regex(/^[A-Za-z\d-_]{128}$/i).messages(MESSAGES.format_not_valid)
    })
};

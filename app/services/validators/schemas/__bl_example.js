const Joi = require('@hapi/joi');
const { MESSAGES } = require('../messages');

module.exports = {
    create: Joi.object({
        name: Joi.string()
            .trim()
            .required().messages(MESSAGES.is_required),
        description: Joi.string()
            .trim()
            .required().messages(MESSAGES.is_required),
        price: Joi.number()
            .integer()
            .required().messages(MESSAGES.is_required)
    }),
    update: Joi.object({
        name: Joi.string()
            .trim()
            .required().messages(MESSAGES.is_required),
        description: Joi.string()
            .trim()
            .required().messages(MESSAGES.is_required),
        price: Joi.number()
            .integer()
            .required().messages(MESSAGES.is_required)
    })
};

'use strict';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const schemas = {};
fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        let name = file.split('.')[0];
        schemas[name] = {...require(path.join(__dirname, file))};
    });


module.exports = {
    // CORE
    // ...
    'CREATE_example': schemas.__bl_example.create,
    'UPDATE_example/:id': schemas.__bl_example.update,
};

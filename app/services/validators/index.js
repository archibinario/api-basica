const schemas = require('./schemas');
const {
    VALIDATION_OPTIONS,
    SECURITY } = require('../../config/index');

exports.validator = (req) => {
    return new Promise( async (resolve, reject) => {
        try {
            if (Object.keys(req.params).length > 0) {
                let response = await paramsValidator(req.params);
                // ...
            }
            req.__clean_url = '...';
            let schema_validation = req.__clean_url.method + '_' + req.__clean_url.path;
            // ...
            // ...
            if (['POST', 'PUT'].includes(req.method)) {
                req.body = await bodyValidator(req.body, schema_validation, req.__language);
            }
            resolve();
        } catch (error) {
            reject(error);
        }
    });
};

function paramsValidator(data) {
    return new Promise( async (resolve, reject) => {
        try {
            ///...
            return resolve(result);
        } catch (error) {
            return reject(error);
        }
    });
}

function bodyValidator(body, schema_id, __language){
    return new Promise( async (resolve, reject) => {
        try {
            // ...
            resolve(modifiedBody);
        } catch (error) {
            reject(error);
        }
    });
}

exports.uriValidator = (uri) => {
    return new Promise( async (resolve, reject) => {
        try {
            // ...
            resolve();
        } catch (error) {
            reject(error);
        }
    });
};

exports.apiKeyValidator = (data) => {
    return new Promise( async (resolve, reject) => {
        try {
            // ...
            return resolve(result);
        } catch (error) {
            reject(error);
        }
    });
};

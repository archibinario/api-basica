const {
    parseError,
    joiErrorParser,
    mongooseErrorParser,
    mongooseErrorVersioningParser } = require('./error-parsers');

exports.prepareErrorResponse = (error, req, res) => {
    return new Promise( async (resolve) => {
        try {
            //...
            resolve(response);
        } catch(e) {
            e.name = 'INTERNAL_ERROR';
            global.__logger.error(e);
            return res.status(500).json({
                error: true,
                code: 500,
                message: 'INTERNAL_ERROR',
                url: req.originalUrl,
                method: req.method,
                errors: []
            });
        }
    });
};

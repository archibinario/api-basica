/* eslint-disable no-unused-vars */
const { prepareErrorResponse } = require('./handlers');

function error404(req, res, next) {
    throw new Error('URL_NOT_FOUND');
}

async function errorHandler(err, req, res, next) {
    let response = await prepareErrorResponse(err, req, res);
    next({ ...response });
}

function errorResponse(err, req, res, next) {
    res.set('Language', req.__language);
    return res.status(err.code).json(err);
}

module.exports = {
    error404, errorHandler, errorResponse
};

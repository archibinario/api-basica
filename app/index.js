const path = require('path');
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const app = express();

const oas_api_document = require('./caas-v1.json');
const { ENV, PROTOCOL, DOMAIN, PORT, JWT, SETUP } = require('./config');
const { connectDB, disconnectDB, dbReady } = require('./config/mongo');
const { logger } = require('./config/logger');
const errors = require('./services/errors');
const api = require('./routes');
const {
    checkURI,
    checkMaintenance,
    checkCache,
    basicAuth,
    responseMode,
    registerRequest,
    apikey } = require('./middlewares/common');

global.__logger = logger;
global.__appRoot = path.resolve(__dirname, '../');
global.__DOMAIN = (ENV === 'development')
    ? `${PROTOCOL}://${DOMAIN}:${PORT}`
    : `${PROTOCOL}://${DOMAIN}`;

app.set('trust proxy', 1);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use((req, res, next) => {
    if (!req.accepts('application/json')) {
        next(new Error('ACCEPT_HEADER_NOT_VALID'));
    }
    if (dbReady() === false) {
        return next(new Error('DB_IS_DOWN'));
    }
    if (req.path === '/') {
        return res.redirect('/api-docs');
    }
    res.__response = require('./services/response').successResponse;
    next();
});

app.use(responseMode);
app.use(registerRequest);
app.use(checkMaintenance);
app.use('/api-docs', basicAuth, swaggerUi.serve, swaggerUi.setup(oas_api_document));
app.use(apikey);
app.use(checkURI);
app.use(checkCache);
app.use('/api/v1', api);
app.use(errors.error404);
app.use(errors.errorHandler);
app.use(errors.errorResponse);

let server = app.listen(PORT, async () => {
    try {
        if (JWT.EX < 300) {
            throw new Error('JWT_TTL_TOO_SHORT');
        }
        await connectDB();
        if (SETUP === true) {
            console.log('\nStarting setup...\n');
            const { setup } = require('./lib/setup');
            setup(app);
        }
        if (ENV === 'development') {
            console.log(`app listening port ${PORT} on ${ENV}`);
            console.log(`jwt security is enable with ${JWT.EX/60} minutes TTL`);
            console.log('db connected');
        }
    } catch (error) {
        global.__logger.error('FAIL_STARTING_APP', { error: error });
        closeApp(error);
    }
});

process
    .on('uncaughtException', err => global.__logger.error('UNCAUGHT_EXCEPTION', { error: err }) )
    .on('unhandledRejection', (err, p) => global.__logger.error('UNHANDLED_REJECTION', { error: err, promise: p }) )
    .on('SIGTERM', closeApp)
    .on('SIGINT', closeApp);

function closeApp(error = '') {
    console.log('CLOSING APP', error);
    global.__logger.info('CLOSING_HTTP_SERVER', {});
    server.close(async () => {
        global.__logger.info('HTTP_SERVER_CLOSED', {});
        await disconnectDB();
        process.exit(0);
    });
}

const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    name: { type: String },
    description: { type: String },
    language: { type: String },
    book: { type: mongoose.Schema.Types.ObjectId, ref: 'Book' },
    price: { type: Number },
}, {
    timestamps :{
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    optimisticConcurrency: true
});

module.exports = mongoose.model('Article', schema, 'articles');

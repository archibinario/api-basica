const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const schema = new Schema({
    title: {type: String},
    language: { type: String },
    year: {type: Number}
}, {
    timestamps :{
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    optimisticConcurrency: true
});

module.exports = mongoose.model('Book', schema, 'books');

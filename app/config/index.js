require('dotenv').config();

const config = {
    ENV: process.env.NODE_ENV || 'development',
    NODE_ENV: process.env.NODE_ENV || 'development',
    SETUP: process.env.SETUP_INIT ? JSON.parse(process.env.SETUP_INIT) : false,
    ADMIN_EMAIL: process.env.ADMIN_EMAIL,
    ADMIN_FIRST_NAME: process.env.ADMIN_FIRST_NAME,
    ADMIN_LAST_NAME: process.env.ADMIN_LAST_NAME,
    LOG_PATH: process.env.LOG_PATH || './log',
    PROTOCOL: process.env.PROTOCOL || 'http',
    DOMAIN: process.env.DOMAIN,
    PORT: process.env.PORT,
    PROJECT_NAME: process.env.PROJECT_NAME,
    PROJECT_EMAIL: process.env.PROJECT_EMAIL,
    PROJECT_VERSION: process.env.PROJECT_VERSION,
    API_DOC_PATH: process.env.API_DOC_PATH || '/api-docs',
    API_DOC_ENABLED: process.env.API_DOC_ENABLED
        ? JSON.parse(process.env.API_DOC_ENABLED)
        : true,

    VALIDATION_OPTIONS: {
        abortEarly: false,
        convert: true,
        allowUnknown: false,
        stripUnknown: true,
        errors: {
            escapeHtml: true,
            label: false,
        },
    },
    LANGUAGE: {
        DEFAULT_LANGUAGE: process.env.DEFAULT_LANGUAGE || 'es',
        VALID_LANGUAGES: process.env.VALID_LANGUAGES
            ? process.env.VALID_LANGUAGES.split(',')
            : ['es-ES', 'en-US'],
        MULTILANGUAGE: process.env.MULTILANGUAGE
            ? JSON.parse(process.env.MULTILANGUAGE)
            : false,
        ALLOW_SWITCH_RESPONSE_MODE: process.env.ALLOW_SWITCH_RESPONSE_MODE
            ? JSON.parse(process.env.ALLOW_SWITCH_RESPONSE_MODE)
            : true,
        RESPONSE_MODE_AS_CONSTANT: 'CONSTANT',
    },
    DB: {
        HOST: process.env.DB_HOST,
        PORT: process.env.DB_PORT,
        NAME: process.env.DB_NAME,
        USERNAME: process.env.DB_USERNAME,
        PASS: process.env.DB_PASS,
        DIALECT: process.env.DB_DIALECT,
    },
    DB_TEST: {
        HOST: process.env.DB_TEST_HOST,
        PORT: process.env.DB_TEST_PORT,
        NAME: process.env.DB_TEST_NAME,
        USERNAME: process.env.DB_TEST_USERNAME,
        PASS: process.env.DB_TEST_PASS,
        DIALECT: process.env.DB_TEST_DIALECT,
        STORAGE: process.env.DB_TEST_STORAGE,
    },
    MONGODB: {
        CONNECTION_STRING: process.env.MONGODB_CONNECTION_STRING,
    },
    REDIS: {
        SECURE_CONN: process.env.REDIS_SECURE_CONN
            ? JSON.parse(process.env.REDIS_SECURE_CONN)
            : false,
        USER: process.env.REDIS_USER,
        PASS: process.env.REDIS_PASS,
        HOST: process.env.REDIS_HOST,
        PORT: process.env.REDIS_PORT,
    },
    REDIS_PREFIXES: {
        CONFIG_MAINTENANCE:
            process.env.MAINTENANCE_CONFIG_PREFIX || 'config:maintenance',
        CONFIG_CACHE: process.env.CACHE_CONFIG_PREFIX || 'config:cache',
        CONFIG_BAN: process.env.BAN_CONFIG_PREFIX || 'config:ban',
        CACHE: process.env.CACHE_PREFIX || 'cache:',
        CACHE_TTL: process.env.CACHE_TTL ? parseInt(process.envCACHE_TTL) : 20,
        BAN: process.env.BAN_PREFIX || 'ban:',
    },
    JWT: {
        SECRET:
            process.env.JWT_SECRET ||
            'a secret',
        EX: process.env.JWT_EXPIRATION
            ? Math.round(parseInt(process.env.JWT_EXPIRATION))
            : 0,
        OFFSET: 60,
    },
    SECURITY: {
        SALT: process.env.SALT_PASS ? parseInt(process.env.SALT_PASS) : 10,
        MAX_ATTEMPTS_LOGIN: process.env.MAX_ATTEMPTS_LOGIN
            ? parseInt(process.env.MAX_ATTEMPTS_LOGIN)
            : 3,
        URI_INVALID_CHARS: process.env.URI_INVALID_CHARS
            ? new RegExp(process.env.URI_INVALID_CHARS, 'igm')
            : new RegExp(/[+%@%~|!:;,.()]/, 'igm'),
        MAX_URI_LENGTH: process.env.MAX_URI_LENGTH
            ? parseInt(process.env.MAX_URI_LENGTH)
            : 150,
        SAFE_IP: process.env.SAFE_IP
            ? process.env.SAFE_IP.split(',')
            : ['127.0.0.1', '::1'],
        FRONT_IP: process.env.FRONT_IP
            ? process.env.FRONT_IP.split(',')
            : ['127.0.0.1', '::1'],
        FRONT_TOKEN: process.env.FRONT_TOKEN,
        PREREGISTER_TTL: process.env.PREREGISTER_TTL
            ? parseInt(process.env.PREREGISTER_TTL)
            : 86400,
        PREAUTH_TOKEN_TTL: process.env.JWT_PREAUTH_TOKEN_TTL
            ? parseInt(process.env.JWT_PREAUTH_TOKEN_TTL)
            : 300,
        API_DOC_USERNAME: process.env.API_DOC_USERNAME,
        API_DOC_PASSWORD: process.env.API_DOC_PASSWORD,
        TOKEN_LENGTH: 72,
        _2FA_BACKUP_CODES_NUMBER: 6,
        _2FA_BACKUP_CODES_LENGTH: process.env._2FA_BACKUP_CODES_LENGTH
            ? parseInt(process.env._2FA_BACKUP_CODES_LENGTH)
            : 12,
        SUSPECT_BAN_ACTIVE: process.env.SUSPECT_BAN_ACTIVE
            ? JSON.parse(process.env.SUSPECT_BAN_ACTIVE)
            : true,
    },
    EMAIL: {
        EMAIL_PROVIDERS: process.env.EMAIL_PROVIDERS
            ? process.env.EMAIL_PROVIDERS.split(',')
            : ['SANDBOX', 'SES', 'MAILGUN', 'SENDGRID'],
        EMAIL_PROVIDER: process.env.EMAIL_PROVIDER || 'SANDBOX',
        SMTP: {
            HOST: process.env.EMAIL_HOST,
            PORT: process.env.EMAIL_PORT
                ? parseInt(process.env.EMAIL_PORT)
                : 2525,
            USERNAME: process.env.EMAIL_USERNAME,
            PASSWORD: process.env.EMAIL_PASSWORD,
            TLS: process.env.EMAIL_TLS
                ? JSON.parse(process.env.EMAIL_TLS)
                : false,
            SSL: process.env.EMAIL_SSL
                ? JSON.parse(process.env.EMAIL_SSL)
                : false,
            REJECT_UNAUTHORIZED: process.env.EMAIL_REJECT_UNAUTHORIZED
                ? JSON.parse(process.env.EMAIL_REJECT_UNAUTHORIZED)
                : false,
        },
        SANDBOX: {
            HOST: process.env.EMAIL_SANDBOX_HOST,
            PORT: process.env.EMAIL_SANDBOX_PORT
                ? parseInt(process.env.EMAIL_SANDBOX_PORT)
                : 2525,
            USERNAME: process.env.EMAIL_SANDBOX_USERNAME,
            PASSWORD: process.env.EMAIL_SANDBOX_PASSWORD,
            TLS: process.env.EMAIL_SANDBOX_TLS
                ? JSON.parse(process.env.EMAIL_SANDBOX_TLS)
                : false,
            SSL: process.env.EMAIL_SANDBOX_SSL
                ? JSON.parse(process.env.EMAIL_SANDBOX_SSL)
                : false,
            REJECT_UNAUTHORIZED: process.env.EMAIL_SANDBOX_REJECT_UNAUTHORIZED
                ? JSON.parse(process.env.EMAIL_SANDBOX_REJECT_UNAUTHORIZED)
                : false,
        },
        MAILGUN: {
            API_KEY: process.env.MAILGUN_API_KEY,
            DOMAIN: process.env.MAILGUN_DOMAIN,
        },
        SENDGRID: {
            API_KEY: process.env.SENDGRID_API_KEY,
        },
        SES: {
            API_KEY: process.env.SES_API_KEY,
        }
    },

    FIREBASE: {
        PROJECT_ID: process.env.FIREBASE_PROJECT_ID,
        CLIENT_EMAIL: process.env.FIREBASE_CLIENT_EMAIL,
        PRIVATE_KEY: process.env.FIREBASE_PRIVATE_KEY,
        DATABASE_URL: process.env.FIREBASE_DATABASE_URL,
    },
    OAUTH: {
        GOOGLE: {
            CLIENT_ID: process.env.OAUTH_GOOGLE_CLIENT_ID,
            CLIENT_SECRET: process.env.OAUTH_GOOGLE_CLIENT_SECRET,
            CALLBACK_URL: process.env.OAUTH_GOOGLE_CALLBACK_URL,
        },
        FACEBOOK: {
            APP_ID: process.env.OAUTH_FACEBOOK_APP_ID,
            APP_SECRET: process.env.OAUTH_FACEBOOK_APP_SECRET,
            CALLBACK_URL: process.env.OAUTH_FACEBOOK_CALLBACK_URL,
        },
        TWITTER: {
            CONSUMER_KEY: process.env.OAUTH_TWITTER_CONSUMER_KEY,
            CONSUMER_SECRET: process.env.OAUTH_TWITTER_CONSUMER_SECRET,
            CALLBACK_URL: process.env.OAUTH_TWITTER_CALLBACK_URL,
        },
        INSTAGRAM: {
            CLIENT_ID: process.env.OAUTH_INSTAGRAM_CLIENT_ID,
            CLIENT_SECRET: process.env.OAUTH_INSTAGRAM_CLIENT_SECRET,
            CALLBACK_URL: process.env.OAUTH_INSTAGRAM_CALLBACK_URL,
        },
    },
    S3: {
        S3_ACCESS_KEY: process.env.S3_ACCESS_KEY,
        S3_SECRET_KEY: process.env.S3_SECRET_KEY,
        S3_BUCKET: process.env.S3_BUCKET,
        S3_REGION: process.env.S3_REGION,
        S3_EXPIRES: parseInt(process.env.S3_EXPIRES) || 60,
    }
};

module.exports = config;
